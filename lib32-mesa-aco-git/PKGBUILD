# $Id: PKGBUILD 59539 2011-11-27 22:55:28Z lcarlier $
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Andreas Radke <andyrtr@archlinux.org>

pkgname=lib32-mesa-aco-git
pkgdesc="mesa with the ACO compiler patchset (32-bit) (git version)"
epoch=1
pkgver=20.1.0_devel.122782.4dc12063961
pkgrel=1
arch=('x86_64')
makedepends=('python-mako' 'lib32-libxml2' 'lib32-expat' 'lib32-libx11' 'xorgproto' 'lib32-libdrm'
             'lib32-libxrandr' 'lib32-libxshmfence' 'lib32-libxxf86vm' 'lib32-libxdamage' 'gcc-multilib' 'lib32-libelf' 'lib32-llvm'
             'lib32-systemd' 'lib32-libvdpau' 'lib32-libva' 'lib32-wayland' 'wayland-protocols' 'lib32-libglvnd' 'lib32-lm_sensors' 
             'meson' 'glslang' 'valgrind' 'lib32-vulkan-icd-loader' 'lib32-zstd' 'git')
url="http://mesa3d.sourceforge.net"
license=('custom')
source=('mesa-aco::git+https://github.com/daniel-schuermann/mesa'
        'crossfile.ini'
        'LICENSE'
        'mesa.patch')
sha256sums=('SKIP'
            '3ea259740141b862e152e07c58f05cad539680541dc181a7233be0c93414e6fb'
            '7052ba73bb07ea78873a2431ee4e828f4e72bda7d176d07f770fa48373dec537'
            '30fa37fccb6e82f4c1513f09e009f1bc15dbae221e3b638dfc2475da847aff1e')

pkgver() {
  cd ${srcdir}/mesa-aco

  read -r _ver <VERSION
  echo ${_ver/-/_}.$(git rev-list --count HEAD).$(git rev-parse --short HEAD)
}

prepare() {
  cd ${srcdir}/mesa-aco

  #patch -Np1 -i ../mesa.patch
}

build() {
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"
  
  arch-meson mesa-aco build \
    --native-file crossfile.ini \
    --libdir=/usr/lib32 \
    -D b_lto=true \
    -D b_ndebug=true \
    -D platforms=x11,wayland,drm,surfaceless \
    -D dri-drivers=i915,i965,r100,r200,nouveau \
    -D gallium-drivers=r300,r600,radeonsi,nouveau,iris,zink,virgl,svga,swrast \
    -D vulkan-drivers=amd,intel \
    -D vulkan-overlay-layer=true \
    -D swr-arches=avx,avx2 \
    -D dri3=true \
    -D egl=true \
    -D gallium-extra-hud=true \
    -D gallium-nine=true \
    -D gallium-omx=disabled \
    -D gallium-opencl=disabled \
    -D gallium-va=true \
    -D gallium-vdpau=true \
    -D gallium-xa=true \
    -D gallium-xvmc=false \
    -D gbm=true \
    -D gles1=false \
    -D gles2=true \
    -D glvnd=true \
    -D glx=dri \
    -D libunwind=false \
    -D llvm=true \
    -D lmsensors=true \
    -D osmesa=gallium \
    -D shared-glapi=true \
    -D valgrind=true

  # Print config
  meson configure build

  ninja -C build xmlpool-pot xmlpool-update-po xmlpool-gmo
  ninja -C build
}

package() {
  depends=('mesa-aco-git' 'lib32-llvm-libs' 'lib32-gcc-libs' 'lib32-libdrm' 'lib32-wayland' 'lib32-libxxf86vm'
           'lib32-libxdamage' 'lib32-libxshmfence' 'lib32-elfutils' 'lib32-libunwind' 'lib32-lm_sensors')
  provides=('lib32-vulkan-intel' 'lib32-vulkan-radeon' 'lib32-vulkan-driver' 'lib32-vulkan-mesa-layer')
  conflicts=('lib32-vulkan-intel' 'lib32-vulkan-radeon' 'lib32-vulkan-radeon-git' 'lib32-vulkan-intel-git' 'lib32-vulkan-mesa-layer-git')
  provides+=('lib32-mesa' 'lib32-mesa-vdpau' 'lib32-mesa-dri'  'lib32-mesa-libgl' 'lib32-opengl-driver')
  conflicts+=('lib32-mesa' 'lib32-mesa-vdpau' 'lib32-mesa-libgl')

  DESTDIR="${pkgdir}" ninja -C build install

  # indirect rendering
  ln -s /usr/lib32/libGLX_mesa.so.0 "${pkgdir}/usr/lib32/libGLX_indirect.so.0"
  
  rm -rv "${pkgdir}"/usr/share/vulkan/explicit_layer.d
  rm -rv "${pkgdir}"/usr/share/drirc.d
  rm -rv "${pkgdir}"/usr/share/glvnd
  rm -rv "${pkgdir}"/usr/include

  install -m644 -Dt "${pkgdir}/usr/share/licenses/${pkgname}" LICENSE
}
