# Maintainer: Laurent Carlier <lordheavym@gmail.com>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Andreas Radke <andyrtr@archlinux.org>

pkgname=mesa-aco-git
pkgdesc="mesa with the ACO compiler patchset (git version)"
epoch=1
pkgver=20.1.1.123293.951983c483a
pkgrel=1
arch=('x86_64')
makedepends=('python-mako' 'libxml2' 'libx11' 'libdrm' 'xorgproto' 'libxrandr'
	     'libxshmfence' 'libxxf86vm' 'libxdamage' 'libvdpau' 'libva' 'wayland' 'wayland-protocols' 'elfutils' 'llvm' 'systemd'
	     'libomxil-bellagio' 'libglvnd' 'libunwind' 'lm_sensors' 'meson' 'libclc' 'clang' 'glslang' 'valgrind' 'zstd'
	     'vulkan-icd-loader' 'git')
url="http://mesa3d.sourceforge.net"
license=('custom')
source=('mesa-aco::git+https://github.com/daniel-schuermann/mesa'
        'LICENSE'
        'mesa.patch')
sha256sums=('SKIP'
            '7fdc119cf53c8ca65396ea73f6d10af641ba41ea1dd2bd44a824726e01c8b3f2'
            'SKIP')

pkgver() {
  cd ${srcdir}/mesa-aco

  read -r _ver <VERSION
  echo ${_ver/-/_}.$(git rev-list --count HEAD).$(git rev-parse --short HEAD)
}

prepare() {
  cd ${srcdir}/mesa-aco

  #patch -Np1 -i ../mesa.patch
}

build() {
  arch-meson mesa-aco build \
    -D b_lto=true \
    -D b_ndebug=true \
    -D platforms=x11,wayland,drm,surfaceless \
    -D dri-drivers=i915,i965,r100,r200,nouveau \
    -D gallium-drivers=r300,r600,radeonsi,nouveau,iris,zink,virgl,svga,swrast \
    -D vulkan-drivers=amd,intel \
    -D vulkan-overlay-layer=true \
    -D swr-arches=avx,avx2 \
    -D dri3=true \
    -D egl=true \
    -D gallium-extra-hud=true \
    -D gallium-nine=true \
    -D gallium-omx=bellagio \
    -D gallium-opencl=icd \
    -D gallium-va=true \
    -D gallium-vdpau=true \
    -D gallium-xa=true \
    -D gallium-xvmc=false \
    -D gbm=true \
    -D gles1=false \
    -D gles2=true \
    -D glvnd=true \
    -D glx=dri \
    -D libunwind=true \
    -D llvm=true \
    -D lmsensors=true \
    -D osmesa=gallium \
    -D shared-glapi=true \
    -D valgrind=true

  # Print config
  meson configure build

  ninja -C build xmlpool-pot xmlpool-update-po xmlpool-gmo
  ninja -C build
}

package() {
  depends=('libdrm' 'libxxf86vm' 'libxdamage' 'libxshmfence' 'libelf' 'llvm-libs' 'clang'
           'libomxil-bellagio' 'libunwind' 'libglvnd' 'wayland' 'lm_sensors' 'libclc' 'glslang')
  provides=('opencl-mesa' 'opencl-driver')
  conflicts=('opencl-mesa' 'opencl-mesa-git')
  optdepends=('opencl-headers: headers necessary for OpenCL development')
  provides+=('vulkan-intel' 'vulkan-radeon' 'vulkan-driver')
  conflicts+=('vulkan-intel' 'vulkan-intel-git' 'vulkan-radeon' 'vulkan-radeon-git')
  provides+=('vulkan-mesa-layer')
  conflicts+=('vulkan-mesa-layer' 'vulkan-mesa-layer-git')
  provides+=('mesa' 'opengl-driver')
  conflicts+=('mesa')
  optdepends+=('opengl-man-pages: for the OpenGL API man pages')

  DESTDIR="${pkgdir}" ninja -C build install

  # in vulkan-headers
  rm -rv "${pkgdir}"/usr/include/vulkan

  rm -rv "${pkgdir}"/usr/bin
 
  # indirect rendering
  ln -s /usr/lib/libGLX_mesa.so.0 "${pkgdir}/usr/lib/libGLX_indirect.so.0"

  install -m644 -Dt "${pkgdir}/usr/share/licenses/${pkgname}" LICENSE
}
